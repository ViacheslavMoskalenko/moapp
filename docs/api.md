## Movies search
* Url: /api/movies/search
* Query params:
    * clue - text phrase to be used for a full text search
    * page - page number
    * size - chunk size to be returned back if match found
* Example: /api/movies/search?clue=doctor+who&page=0&size=5
* Returned data structure:
`{items: [Movie], total: Number}`

## Movie details
* Url: /api/movies/:id
* Example: /api/movies/5b45e8d08b1599d0e98ec1bc
* Returned data structure:
`{actors: [String],_id: String, year: Number,title: Number, locations: String,funFacts :String, productionCompany: String,distributor: String, director: String, writer: String}`
