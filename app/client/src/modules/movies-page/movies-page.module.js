import angular from 'angular';
import uiRouter from 'angular-ui-router';
import utilsModule from '../utils/utils.module';
import MoviesListComponent from './components/movies-list/movies-list.component';
import FetchMoreComponent from './components/fetch-more/fetch-mode.component';

const moviesPageModule = angular.module('moviesPage', [
    uiRouter,
    utilsModule
]);
moviesPageModule.component('moviesList', MoviesListComponent)
    .config(($stateProvider, $urlRouterProvider) => {
        const moviesListUrl = '/movies-list';
        $urlRouterProvider.otherwise(moviesListUrl);
        $stateProvider.state('moviesList', {
            template: '<movies-list></movies-list>',
            url: moviesListUrl
        });
    })
    .component('fetchMore', FetchMoreComponent);

export default moviesPageModule.name;
