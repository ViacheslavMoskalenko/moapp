/**
 * Encapsulates fetch more button controller
 */
class FetchMoreComponentCtrl {}

const fetchMoreComponent = {
    controller: FetchMoreComponentCtrl,
    template: require('./fetch-more.component.html'),
    controllerAs: '$fetchMoreCtrl',
    bindings: {
        fetching: '<'
    }
};

export default fetchMoreComponent;
