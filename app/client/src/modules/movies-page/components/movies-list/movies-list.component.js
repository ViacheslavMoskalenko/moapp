/**
 * Encapsulates movies list controller
 */
class MoviesListComponentCtrl {
    /**
     * @param {Object} movies movies service
     */
    constructor(movies) {
        this._moviesService = movies;
        this.movies = [];
        this.clue = '';
        this.fetching = false;
        this.page = {
            id: 0,
            size: 5
        };
    }

    /**
     * Resets component data
     */
    reset() {
        this.page.id = 0;
        this.movies = [];
    }

    /**
     * Fetches a list of movies
     * @return {void}
     */
    fetch() {
        if (this.fetching) {
            return console.warn('Movies are already fetching. Doing nothing');
        }
        this.fetching = true;
        this._moviesService.fetchMovies(this.clue, this.page.id, this.page.size)
        .then(data => {
            this.movies = this.movies.concat(data);
            ++this.page.id;
        })
        .catch(err => console.error('Unable to fetch a list of movies: ', err))
        .finally(() => {
            this.fetching = false;
        });
    }

    /**
     * Reacts on clue changes
     */
    reactOnClueChange() {
        this.reset();
        this.fetch();
    }
}

const moviesListComponent = {
    controller: MoviesListComponentCtrl,
    template: require('./movies-list.template.html'),
    controllerAs: '$moviesListCtrl'
};

export default moviesListComponent;
