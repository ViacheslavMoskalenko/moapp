import angular from 'angular';
import moviesPageModule from '../movies-page/movies-page.module';
import movieDetailsModule from '../movie-details/movie-details.module';
import 'bootstrap/dist/css/bootstrap.css';

const mainModule = angular.module('main', [
    moviesPageModule,
    movieDetailsModule
]);
mainModule.config(($compileProvider, $locationProvider) => {
    $locationProvider.html5Mode(true);
    $compileProvider.debugInfoEnabled(false);
    $compileProvider.commentDirectivesEnabled(false);
    $compileProvider.cssClassDirectivesEnabled(false);
});
mainModule.run($rootScope => {
    $rootScope.title = 'Moapp - movie app';
});

export default mainModule;
