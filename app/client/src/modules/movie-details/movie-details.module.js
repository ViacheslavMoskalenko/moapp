import angular from 'angular';
import uiRouter from 'angular-ui-router';
import utilsModule from '../utils/utils.module';
import movieDetailsComponent from './components/movie-details/movie-details.component';

const movieDetailsModule = angular.module('movieDetails', [
    uiRouter,
    utilsModule
]);

movieDetailsModule.config($stateProvider => {
    $stateProvider.state('movieDetails', {
        template: '<movie-details></movie-details>',
        url: '/movie/:id'
    });
})
.component('movieDetails', movieDetailsComponent);

export default movieDetailsModule.name;
