/**
 * Encapsulates movies details controller
 */
class MovieDetailsComponentCtrl {
    /**
     * @param {Object} movies movies service
     * @param {Object} $stateParams state parameters
     */
    constructor(movies, $stateParams) {
        this._moviesService = movies;
        this._stateParams = $stateParams;
        this.movie = {};
    }

    /**
     * Fetches a movie details
     * @param {String} movieId movie unique identifier
     * @return {void}
     */
    fetch(movieId) {
        if (!movieId) {
            return console.warn('Movie id is unknown. Doing nothing.');
        }
        this._moviesService.fetchMovieDetails(movieId)
        .then(movie => this.movie = movie)
        .catch(err => console.error('Unable to fetch movie details: ', err));
    }

    /**
     * Returns movie unique identifier
     * @return {String} movie id
     * @private
     */
    _getMovieId() {
        return this._stateParams.id || '';
    }

    /**
     * Fetches movie details on init
     * lifecycle hook
     */
    $onInit() {
        this.fetch(this._getMovieId());
    }

    /**
     * Checks if actors are presented or not
     * @param {Array<Object>} actors a list of actors
     * @return {Boolean} flag indicating if there is
     * at least one actor in actors list
     */
    actorsArePresented(actors) {
        return actors.some(actor => !!actor);
    }
}

const movieDetailsComponent = {
    controller: MovieDetailsComponentCtrl,
    template: require('./movie-details.template.html'),
    controllerAs: '$movieDetailsCtrl'
};

export default movieDetailsComponent;
