/**
 * Encapsulates details of handling
 * movies information
 */
class MoviesService {
    /**
     * @param {Object} $http http service
     */
    constructor($http) {
        this._service = $http;
        this._baseUrl = '/api/movies';
    }

    /**
     * Fetches movies list
     * @param {String} clue a string clue
     * @param {Number} page page number
     * @param {Number} size chunk size
     * @return {Array<Object>} a list of movies
     */
    fetchMovies(clue, page, size) {
        const url = `${this._baseUrl}/search?clue=${clue}&page=${page}&size=${size}`;
        return this._service.get(url)
        .then(response => response.data ? response.data.items : []);
    }

    /**
     * Fetches movie details
     * @param {String} id movie details
     * @return {Promise<Object>} promise to fetch
     * movie details
     */
    fetchMovieDetails(id) {
        const url = `${this._baseUrl}/${id}`;
        return this._service.get(url)
            .then(response => response.data);
    }
}

export default MoviesService;
