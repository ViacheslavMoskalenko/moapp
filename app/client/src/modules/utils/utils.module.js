import angular from 'angular';
import moviesService from './services/movies.service';

const utilsModule = angular.module('utils', []);
utilsModule.config($httpProvider => {
    $httpProvider.defaults.headers.common['Accept'] = 'application/json';
    $httpProvider.defaults.headers.common['Cache-Control'] = 'No-Cache';
    $httpProvider.defaults.headers.common['Expires'] = '-1';
});

utilsModule.service('movies', moviesService);

export default utilsModule.name;
