const apiRouter = require('./api');

/**
 * Bind the application routes
 * @param {Object} app the application instance
 */
function bindRoutes(app) {
    app.use('/api', apiRouter);
}

module.exports = bindRoutes;
