/* eslint-disable-next-line new-cap */
const apiRouter = require('express').Router();
const moviesRouter = require('./movies');

apiRouter.use('/movies', moviesRouter);

module.exports = apiRouter;
