const movieStubs = require('../stubs');
const MovieModel = require('../../../../models/movie');
const TestBench = require('../../../../services/test-bench');
const request = require('request');
const urlStubService = require('../stubs').urlStubService;

describe('Movie detail api ->', () => {
    const testBench = new TestBench('Movie detail handler test');

    beforeAll(async () => {
        try {
            await testBench.launchInfrastructure();
            await MovieModel.insertMany([movieStubs.singleMovie]);
        } catch (err) {
            throw err;
        }
    });

    it('should return a movie by id', done => {
        const movieId = movieStubs.singleMovie._id;
        const url = urlStubService.getMovieDetailUrl(movieId);
        request.get(url, {json: true}, (err, response) => {
            if (err) {
                throw err;
            }
            expect(response.statusCode).toBe(200);
            expect(response.body).toEqual(movieStubs.singleMovie);
            done();
        });
    });

    it('should return a empty object in case of an incorrect id', done => {
        const movieId = `${movieStubs.singleMovie._id}_${Math.random()}`;
        const url = urlStubService.getMovieDetailUrl(movieId);
        request.get(url, {json: true}, (err, response) => {
            if (err) {
                throw err;
            }
            expect(response.statusCode).toBe(200);
            expect(response.body).toEqual({});
            done();
        });
    });

    afterAll(async () => {
        try {
            await testBench.dismissInfrastructure();
        } catch (err) {
            console.error('UNABLE TO CLEAN UP');
            throw new Error(err);
        }
    });
});
