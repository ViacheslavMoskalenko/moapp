const moviesService = require('../../../../services/movies');

module.exports = handleMovieDetailRequest;

/**
 * Handles movie detail request and returns
 * all a given movie
 * @param {Object} req request instance
 * @param {Object} res response instance
 */
async function handleMovieDetailRequest(req, res) {
    const movieId = req.params.id;
    try {
        const movie = await moviesService.getMovieById(movieId);
        res.status(200).json(movie);
    } catch (err) {
        if (err.name === 'CastError') {
            return res.status(200).json({});
        }
        const errMsg = 'Server failed to fetch movie details';
        console.error(errMsg, err);
        res.status(500).json({
            message: errMsg
        });
    }
}
