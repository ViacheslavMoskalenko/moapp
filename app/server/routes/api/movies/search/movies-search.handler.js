const moviesService = require('../../../../services/movies');
const PageService = require('../../../../services/page');

module.exports = handleMovieSearchRequest;

/**
 * Handles movie search request and returns
 * all matched results
 * @param {Object} req request instance
 * @param {Object} res response instance
 */
async function handleMovieSearchRequest(req, res) {
    const clue = req.query.clue || '';
    const pageId = +req.query.page;
    const chunkSize = +req.query.size;
    const page = new PageService(pageId, chunkSize);
    try {
        const data = await moviesService.searchMovie(clue, page);
        res.status(200).json(data);
    } catch (err) {
        if (err.badRequest) {
            return res.status(400).json({
                message: 'Incorrect input parameters'
            });
        }
        const errMsg = 'Server failed to fetch movies';
        console.error(errMsg, err);
        res.status(500).json({
            message: errMsg
        });
    }
}
