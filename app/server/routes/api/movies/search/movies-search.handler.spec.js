const movieStubs = require('../stubs');
const MovieModel = require('../../../../models/movie');
const TestBench = require('../../../../services/test-bench');
const request = require('request');
const sortService = require('../../../../services/sort');
const urlStubService = require('../stubs').urlStubService;

describe('Movies search api ->', () => {
    const testBench = new TestBench('Movies search handle test');
    let sortedMoviesStub = sortService.sortAsc(movieStubs.moviesList, 'title');

    beforeAll(async () => {
        try {
            await testBench.launchInfrastructure();
            await MovieModel.insertMany(sortedMoviesStub);
        } catch (err) {
            throw err;
        }
    });

    it('should return chunk of movies', done => {
        const page = 0;
        const size = 2;
        const clue = 'mock';
        const url = urlStubService.getSearchUrl(clue, page, size);
        request.get(url, {json: true}, (err, response) => {
            if (err) {
                throw err;
            }
            expect(response.statusCode).toBe(200);
            expect(response.body.items).toEqual(sortedMoviesStub.slice(0, 2));
            done();
        });
    });

    it('should indicate bad input, when incorrect page provided', done => {
        const page = -1;
        // Size doesn't matter in this case
        const size = 2;
        const clue = 'mock';
        const url = urlStubService.getSearchUrl(clue, page, size);
        request.get(url, {json: true}, (err, response) => {
            if (err) {
                throw err;
            }
            expect(response.statusCode).toBe(400);
            expect(response.body.message).toBe('Incorrect input parameters');
            done();
        });
    });

    it('should indicate bad input, when incorrect chunk size provided', done => {
        const page = 0;
        const size = 0;
        const clue = 'mock';
        const url = urlStubService.getSearchUrl(clue, page, size);
        request.get(url, {json: true}, (err, response) => {
            if (err) {
                throw err;
            }
            expect(response.statusCode).toBe(400);
            expect(response.body.message).toBe('Incorrect input parameters');
            done();
        });
    });

    it('should indicate bad input, when empty clue provided', done => {
        const page = 0;
        const size = 0;
        const clue = '';
        const url = urlStubService.getSearchUrl(clue, page, size);
        request.get(url, {json: true}, (err, response) => {
            if (err) {
                throw err;
            }
            expect(response.statusCode).toBe(400);
            expect(response.body.message).toBe('Incorrect input parameters');
            done();
        });
    });

    it('should indicate bad input, when clue is too long', done => {
        const page = 0;
        const size = 0;
        const clue = 's'.repeat(33);
        const url = urlStubService.getSearchUrl(clue, page, size);
        request.get(url, {json: true}, (err, response) => {
            if (err) {
                throw err;
            }
            expect(response.statusCode).toBe(400);
            expect(response.body.message).toBe('Incorrect input parameters');
            done();
        });
    });

    it('should return next chunk of movies according to a request', done => {
        const page = 1;
        const size = 2;
        const clue = 'mock';
        const url = urlStubService.getSearchUrl(clue, page, size);
        request.get(url, {json: true}, (err, response) => {
            if (err) {
                throw err;
            }
            expect(response.statusCode).toBe(200);
            expect(response.body.items).toEqual(sortedMoviesStub.slice(2, 4));
            done();
        });
    });

    it('should return the exact amount of requested movies', done => {
        const page = 0;
        const size = 5;
        const clue = 'mock';
        const url = urlStubService.getSearchUrl(clue, page, size);
        request.get(url, {json: true}, (err, response) => {
            if (err) {
                throw err;
            }
            expect(response.statusCode).toBe(200);
            expect(response.body.items).toEqual(sortedMoviesStub);
            done();
        });
    });

    it('should return available movies, when it was requested more than that', done => {
        const page = 0;
        const size = 100;
        const clue = 'mock';
        const url = urlStubService.getSearchUrl(clue, page, size);
        request.get(url, {json: true}, (err, response) => {
            if (err) {
                throw err;
            }
            expect(response.statusCode).toBe(200);
            expect(response.body.items).toEqual(sortedMoviesStub);
            done();
        });
    });

    afterAll(async () => {
        try {
            await testBench.dismissInfrastructure();
        } catch (err) {
            throw err;
        }
    });
});
