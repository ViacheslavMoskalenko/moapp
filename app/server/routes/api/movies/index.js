/* eslint-disable-next-line new-cap */
const moviesRouter = require('express').Router();
const movieSearchHandler = require('./search');
const movieDetailHandler = require('./detail');

moviesRouter.get('/search', movieSearchHandler);
moviesRouter.get('/:id', movieDetailHandler);

module.exports = moviesRouter;
