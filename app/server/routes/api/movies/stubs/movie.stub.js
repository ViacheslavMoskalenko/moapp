const MovieModel = require('../../../../models/movie/movie.model');
const dummy = require('mongoose-dummy');
const ignoredFields = require('./ingored-fields.config');
const random = require('../../../../services/random');

/**
 * Encapsulates details of creating mock movies
 */
class MockMovieFactory {
    /**
     * Generates a dummy movie object
     * @param {String=} name name for a mock movie
     * @return {Object} randomly generated movie
     */
    static create(name = 'Mock item') {
        const mockMovie = dummy(MovieModel, {
            ignore: ignoredFields
        });
        mockMovie.title = name;
        mockMovie.year = random.getRandomNumber(1920, 2018);
        return mockMovie;
    }
}

module.exports = MockMovieFactory;
