const MockMovieFactory = require('./movie.stub');
const MockMoviesListFactory = require('./movie-list.stub');
const MoviesStubUrlService = require('./stub-url.service');
module.exports = {
    singleMovie: MockMovieFactory.create(),
    moviesList: MockMoviesListFactory.create(5),
    urlStubService: MoviesStubUrlService
};
