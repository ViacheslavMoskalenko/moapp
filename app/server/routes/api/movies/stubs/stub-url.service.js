const appTestConfig = require('../../../../config').test;
/**
 * Encapsulates details for constructing
 * mock urls for testing purposes
 */
class MoviesStubUrlService {
    /**
     * Constructs request url for search a movies
     * @param {String} clue a search phrase
     * @param {Number} page a page number
     * @param {Number} size a data chunk size
     * @return {String} a request url
     */
    static getSearchUrl(clue, page, size) {
        const parts = [
            `http://localhost:${appTestConfig.app.port}/api/movies/search?clue=${clue}`,
            `page=${page}`,
            `size=${size}`
        ];
        return parts.join('&');
    }

    /**
     * Constructs request url for movie details
     * @param {String} id a movie id
     * @return {String} a request url
     */
    static getMovieDetailUrl(id) {
        return `http://localhost:${appTestConfig.app.port}/api/movies/${id}`;
    }
}

module.exports = MoviesStubUrlService;
