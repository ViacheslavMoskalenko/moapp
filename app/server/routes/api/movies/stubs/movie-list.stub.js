const MockMovieFactory = require('./movie.stub');

/**
 * Encapsulates details of creating a list of mock movies
 */
class MockMoviesListFactory {
    /**
     * @param {Number=} howMany amount of generated movies
     * @return {Array<Object>} a list of mocked movies
     */
    static create(howMany = 5) {
        const list = [];
        for (let i = 0; i < howMany; i++) {
            list.push(MockMovieFactory.create(`Mock movie ${i + 1}`));
        }
        return list;
    }
}

module.exports = MockMoviesListFactory;
