const _ = require('lodash');

/**
 * Internal function for inspecting an object's
 * property value
 * @param {any} target an object
 * @param {String} prop a property name or a property path
 * @return {*} property value
 */
function getPropVal(target, prop) {
    return prop ? _.get(target, prop) : target;
}

/**
 * Encapsulates collection sorting details
 */
class SortService {
    /**
     * Sorts a collection in ascending order
     * by a given property
     * @param {Array<any>} collection a collection of items
     * @param {String=} prop property name of property path
     * @return {Array<any>} a new sorted collection
     */
    static sortAsc(collection, prop = '') {
        const copy = _.cloneDeep(collection);
        copy.sort((lhs, rhs) => {
            const lv = getPropVal(lhs, prop);
            const rv = getPropVal(rhs, prop);

            if (lv < rv) {
                return -1;
            } else if (lv > rv) {
                return 1;
            }
            return 0;
        });
        return copy;
    }

    /**
     * Sorts a collection in descending order
     * by a given property
     * @param {Array<any>} collection a collection of items
     * @param {String=} prop property name of property path
     * @return {Array<any>} a new sorted collection
     */
    static sortDesc(collection, prop = '') {
        const copy = _.cloneDeep(collection);
        copy.sort((lhs, rhs) => {
            const lv = getPropVal(lhs, prop);
            const rv = getPropVal(rhs, prop);

            if (lv < rv) {
                return 1;
            } else if (lv > rv) {
                return -1;
            }
            return 0;
        });
        return copy;
    }
}

module.exports = SortService;
