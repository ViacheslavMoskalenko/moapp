const sortService = require('./sort.service');

describe('Sort service ->', () => {
    it('should sort a collection of primitives in ascending order', () => {
        const unsortedItems = [5, 1, 4, 2, 3];
        const sortedItems = sortService.sortAsc(unsortedItems);
        expect(sortedItems).not.toBe(unsortedItems);
        expect(sortedItems).toEqual([1, 2, 3, 4, 5]);
    });

    it('should sort a collection of primitives in descending order', () => {
        const unsortedItems = [5, 1, 4, 2, 3];
        const sortedItems = sortService.sortDesc(unsortedItems);
        expect(sortedItems).not.toBe(unsortedItems);
        expect(sortedItems).toEqual([5, 4, 3, 2, 1]);
    });

    it('should sort a collection of non-primitives in ascending order', () => {
        const unsortedItems = [
            {
                prop: 5
            },
            {
                prop: 1
            },
            {
                prop: 4
            }
        ];
        const sortedItems = sortService.sortAsc(unsortedItems, 'prop');
        expect(sortedItems).not.toBe(unsortedItems);
        expect(sortedItems).toEqual([
            {
                prop: 1
            },
            {
                prop: 4
            },
            {
                prop: 5
            }
        ]);
    });

    it('should sort a collection of non-primitives in descending order', () => {
        const unsortedItems = [
            {
                prop: 5
            },
            {
                prop: 1
            },
            {
                prop: 4
            }
        ];
        const sortedItems = sortService.sortDesc(unsortedItems, 'prop');
        expect(sortedItems).not.toBe(unsortedItems);
        expect(sortedItems).toEqual([
            {
                prop: 5
            },
            {
                prop: 4
            },
            {
                prop: 1
            }
        ]);
    });
});
