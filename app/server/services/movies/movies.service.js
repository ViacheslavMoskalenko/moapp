const MovieModel = require('../../models/movie');
const CLUE_LIMIT_LEN = 32;

/**
 * Encapsulates CRUD operations with
 * movies collection
 */
class MovieService {
    /**
     * Generates a default movie projection for
     * any consumer
     * @return {Object} an object to be used
     * for projection
     * @private
     */
    static getMovieProjection() {
        return {
            _id: 1,
            year: 1,
            title: 1,
            locations: 1,
            funFacts: 1,
            productionCompany: 1,
            distributor: 1,
            director: 1,
            writer: 1,
            actors: 1
        };
    }
    /**
     * Searches a movie using a clue, which
     * represents any of movie param (year, publisher,
     * author, etc)
     * @param {String} clue parameter to be compared
     * @param {Object} pageParams page navigation parameters
     * @return {Promise<Object>} promise to return matched movies
     */
    searchMovie(clue, pageParams) {
        if (!pageParams.valid() || !clue || clue.length > CLUE_LIMIT_LEN) {
            return Promise.reject({
                badRequest: true
            });
        }
        return MovieModel.aggregate([
            {
                $match: {
                    $text: {
                        $search: clue,
                        $caseSensitive: false
                    }
                }
            },
            {
                $sort: {
                    title: 1
                }
            },
            {
                $project: MovieService.getMovieProjection()
            },
            {
                $group: {
                    _id: {},
                    items: {
                        $push: '$$CURRENT'
                    },
                    total: {
                        $sum: 1
                    }
                }
            },
            {
                $project: {
                    _id: 0,
                    items: {
                        $slice: [
                            '$items',
                            pageParams.page * pageParams.size,
                            pageParams.size
                        ]
                    },
                    total: 1
                }
            }
        ])
        // There will be always only one item in the response
        .then(items => items[0])
        .catch(err => Promise.reject(err));
    }

    /**
     * Returns a specific movie linked to id
     * @param {String} id movie id
     * @return {Promise<Object>} promise to return a movie
     */
    getMovieById(id) {
        return MovieModel.findById(id, MovieService.getMovieProjection());
    }
}

module.exports = new MovieService();
