const PageService = require('./page.service');

describe('Page service ->', () => {
    describe('When checking parameters validity ->', () => {
        const naNs = [
            null,
            undefined,
            'str',
            '',
            function() {},
            [],
            {},
        ];
        const invalidPageIndexes = [
            ...naNs,
            -1,
            Number.MAX_SAFE_INTEGER,
            Number.MAX_SAFE_INTEGER + 1
        ];
        const invalidChunkSizes = [
            ...naNs,
            -1,
            0,
            Number.MAX_SAFE_INTEGER,
            Number.MAX_SAFE_INTEGER + 1
        ];
        invalidPageIndexes.forEach(pageIndex => {
            invalidChunkSizes.forEach(chunk => {
                it(`should be capable to detect incorrect params: ${pageIndex}, ${chunk}`, () => {
                    const page = new PageService(pageIndex, chunk);
                    expect(page.valid()).toBe(false);
                });
            });
        });
        const validPageIndexes = [
            0,
            1,
            Number.MAX_SAFE_INTEGER - 1
        ];
        const validChunks = [
            1,
            Number.MAX_SAFE_INTEGER - 1
        ];
        validPageIndexes.forEach(pageIndex => {
            validChunks.forEach(chunk => {
                it(`should be capable to detect correct params: ${pageIndex}, ${chunk}`, () => {
                    const page = new PageService(pageIndex, chunk);
                    expect(page.valid()).toBe(true);
                });
            });
        });
    });
    describe('When getting access to parameters ->', () => {
        let service = null;

        beforeEach(() => service = new PageService(1, 10));

        it('should return a correct page index', () => {
            expect(service.page).toBe(1);
        });
        it('should return a correct chunk size', () => {
            expect(service.size).toBe(10);
        });
    });
});
