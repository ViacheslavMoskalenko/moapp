/**
 * Encapsulates pageable entity
 */
class PageService {
    /**
     * @param {Number} pageIndex page number
     * @param {Number} chunkSize size of an items
     */
    constructor(pageIndex, chunkSize) {
        this._pageIndex = pageIndex;
        this._chunkSize = chunkSize;
    }

    /**
     * Returns a page index
     * @return {Number} page index
     */
    get page() {
        return this._pageIndex;
    }

    /**
     * Returns a chunk size
     */
    get size() {
        return this._chunkSize;
    }
    /**
     * Checks if pageable parameters are valid
     * returned in a chunk back
     * @return {Boolean} flag indicating if
     * parameters are valid
     */
    valid() {
        if (
            !Number.isFinite(this._pageIndex) ||
            this._pageIndex < 0 ||
            this._pageIndex >= Number.MAX_SAFE_INTEGER
        ) {
            return false;
        }
        if (
            !Number.isFinite(this._chunkSize) ||
            this._chunkSize <= 0 ||
            this._chunkSize >= Number.MAX_SAFE_INTEGER
        ) {
            return false;
        }
        return true;
    }
}

module.exports = PageService;
