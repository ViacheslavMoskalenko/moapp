const appTestConfig = require('../../config').test;
const launchApp = require('../../bootstrap')();

/**
 * Encapsulates details of filling test database with
 * mock data, handling database connection, handling
 * server state
 */
class TestBenchService {
    /**
     * @param {String} testName test name
     */
    constructor(testName) {
        this._testName = testName;
        this._dbCon = null;
        this._server = null;
    }

    /**
     * Bootstraps test server and a database
     * @return {Promise<void>} promise to set
     * up test infrastructure
     */
    async launchInfrastructure() {
        try {
          const appParts = await launchApp({
              db: appTestConfig.db,
              port: appTestConfig.app.port,
              static: appTestConfig.app.static
          });
          console.log(`${this._testName}. Launched test server and test database`);
          this._dbCon = appParts.dbConnection;
          this._server = appParts.server;
            console.log(`${this._testName}. AFTER SERVER IS LAUNCHED`);
        } catch (err) {
            console.error(`${this._testName}. Error: `, err);
            throw err;
        }
    }

    /**
     * Destroys test infrastructure
     * @return {Promise<void>} promise to
     * destroy test infrastructure
     */
    async dismissInfrastructure() {
        try {
            await this._dbCon.db.dropDatabase();
            console.log(`${this._testName}: dropped test database`);
            await this._server.close();
            console.log(`${this._testName}: stopped application test server`);
            await this._dbCon.close(true);
            console.log(`${this._testName}: closed test database connection`);
        } catch (err) {
            console.error(`${this._testName}: unable to clean up`, err);
            throw err;
        }
    }
}

module.exports = TestBenchService;
