/**
 * Encapsulates underlying details
 * for generating random values
 */
class Random {
    /**
     * Generates a random number
     * @param {Number=} min minimum value
     * @param {Number=} max maximum value
     * @return {Number} random number
     */
    static getRandomNumber(min = 0, max = Number.MAX_SAFE_INTEGER) {
        return min + Math.floor(Math.random() * (max - min + 1));
    }
}

module.exports = Random;
