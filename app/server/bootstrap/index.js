const connectToDb = require('../db');
const express = require('express');
const bindRoutes = require('../routes');
const html5ModeMiddleware = require('../middleware').html5;

/**
 * Bootstraps the application
 * @return {Function} launch function
 */
function bootstrapApp() {
    const app = express();
    bindRoutes(app);

    /**
     * Launches the application
     * @param {Object} config a configuration object
     * @param {String} config.static an absolute path to server static files from
     * @param {Number} config.port a port the application should listen to
     * @param {Object} config.db database configuration object
     * @param {String} config.static path to a static resources
     * @param {Function=} config.onLaunch callback to be executed, when
     * server is up and running
     * @return {Object} an object containing application server
     * instance and database connection
     */
    return async config => {
        let dbConnection = null;
        try {
            dbConnection = await connectToDb(config.db);
        } catch (error) {
            console.error('Unable to connect to a database: ', error);
            console.log('Exiting...');
            process.exit(1);
        }
        const defaultOnLaunch = () => {
            console.log(`Server is listening at http://localhost:${config.port}`);
        };
        app.use(express.static(config.static));

        /*
         * Html5 routing mode middleware has
         * to be enabled after static assets
         * middleware. Otherwise express app
         * will send index.html file to each
         * and every http request
         */
        html5ModeMiddleware(app);
        const server = app.listen(
            config.port,
            config.onLaunch || defaultOnLaunch
        );
        return {
            server,
            dbConnection
        };
    };
}

module.exports = bootstrapApp;
