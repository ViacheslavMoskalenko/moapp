const mongoose = require('mongoose');

module.exports = connectToDb;

/**
 * Connects to MongoDb
 * @param {Object} config database connection config
 * @param {String} config.name database name
 * @param {Number} config.port connection port
 * @return {Promise} promise to connect
 * to a database
 */
async function connectToDb(config) {
    try {
        const dbUri = `mongodb://localhost:${config.port}/${config.name}`;
        await mongoose.connect(dbUri, {
            autoIndex: true,
            useNewUrlParser: true
        });
        return mongoose.connection;
    } catch (error) {
        throw error;
    }
}
