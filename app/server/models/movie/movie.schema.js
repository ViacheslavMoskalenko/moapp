const Schema = require('mongoose').Schema;
const movieSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    year: {
        type: Number,
        min: [1900, 'No way cinema was available before 20 century'],
        max: [3000, 'Too far away!']
    },
    locations: {
        type: String,
        required: true
    },
    funFacts: {
        type: String
    },
    productionCompany: {
        type: String,
        required: true
    },
    distributor: {
        type: String,
        required: true
    },
    director: {
        type: String,
        required: true
    },
    writer: {
        type: String,
        required: true
    },
    actors: {
        type: [String]
    }
}, {
    collection: 'movies'
});

movieSchema.index({
    title: 'text',
    year: 'text',
    locations: 'text',
    funFacts: 'text',
    productionCompany: 'text',
    distributor: 'text',
    director: 'text',
    writer: 'text',
    actors: 'text'
}, {
    name: 'movieOmniSearch'
});

module.exports = movieSchema;
