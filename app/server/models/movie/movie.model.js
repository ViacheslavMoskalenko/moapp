const movieSchema = require('./movie.schema');
const Movie = require('mongoose').model('Movie', movieSchema);
module.exports = Movie;
