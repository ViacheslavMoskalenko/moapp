const appConfig = require('./config').app;
const bootstrapApp = require('./bootstrap');
const launchApp = bootstrapApp();

launchApp({
    db: appConfig.db,
    port: appConfig.app.port,
    static: appConfig.app.static
});
