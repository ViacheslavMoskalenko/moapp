const path = require('path');

module.exports = {
    db: {
        name: 'moapp',
        port: 27017
    },
    app: {
        port: process.env.PORT || +process.argv[2] || 3000,
        static: path.join(__dirname, '..', '..', 'client', 'dist')
    }
};
