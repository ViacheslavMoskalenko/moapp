const path = require('path');

module.exports = {
    db: {
        name: 'testmoapp',
        port: 27017
    },
    app: {
        port: process.env.PORT || +process.argv[2] || 5678,
        static: path.join(__dirname, '..', '..', 'client', 'src')
    }
};
