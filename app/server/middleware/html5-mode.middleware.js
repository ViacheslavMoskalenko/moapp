const path = require('path');
module.exports = enableHtml5Mode;

/**
 * Applies middleware enabling
 * html5 routing mode in client side
 * application
 * @param {Object} app application instance
 */
function enableHtml5Mode(app) {
    app.all('/*', (req, res) => {
        res.sendFile('index.html', {
            root: path.join(__dirname, '../../client/dist')
        });
    });
}
