/**
 * Default data modifier adds
 * no mutations
 */
class DefaultDataModifier {
    /**
     * Dummy method, which doesn't
     * alter data in any way
     */
    change() {}
}

module.exports = DefaultDataModifier;
