const csvToJson = require('csvtojson');

/**
 * Provides possibility to read csv
 * data as json
 */
class CsvToJsonDataReadProvider {
    /**
     * Returns a json representation
     * of a data stored in a csv file
     * @param {String} path path to a file
     * @return {Promise<String>} promise to read a file content
     */
    read(path) {
        return csvToJson().fromFile(path)
        .then(data => data)
        .catch(error => {
            console.error('Unable to convert csv into a json: ', error);
            return Promise.reject(error);
        });
    }
}

module.exports = CsvToJsonDataReadProvider;
