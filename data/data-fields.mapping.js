module.exports = {
    'Release Year': 'year',
    Title: 'title',
    Locations: 'locations',
    'Fun Facts': 'funFacts',
    'Production Company': 'productionCompany',
    Distributor: 'distributor',
    Director: 'director',
    Writer: 'writer',
    '>Actor 1,Actor 2,Actor 3': 'actors'
};
