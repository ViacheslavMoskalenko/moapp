const FIELDS_MAPPING = require('./data-fields.mapping');
const oldPropNames = Object.keys(FIELDS_MAPPING);

/**
 * Encapsulates all data mapping from original fields
 * to fields of a new format
 */
class DataFormatModifier {
    /**
     * Modifies data item in place
     * @param {Object} item a movie
     */
    change(item) {
        oldPropNames.forEach(oldName => {
            const newName = FIELDS_MAPPING[oldName];
            // '>' implies aggregation to a collection
            if (oldName.charAt(0) === '>') {
                const oldPropsList = oldName.slice(1).split(',');
                item[newName] = oldPropsList.map(prop => {
                    const value = item[prop];
                    delete item[prop];
                    return value;
                });
            } else {
                item[newName] = item[oldName];
                delete item[oldName];
            }
        });
    }
}

module.exports = DataFormatModifier;
