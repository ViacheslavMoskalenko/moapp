const CsvToJsonDataReadProvider = require('./csv-to-json-data-read.provider');
const DataFormatModifier = require('./data-format.modifier');
const MongoDbDataImporter = require('./mongodb-data.importer');

const mongoDbDataImporter = new MongoDbDataImporter(
    new CsvToJsonDataReadProvider(),
    new DataFormatModifier()
);

mongoDbDataImporter.importData('movies.csv');
