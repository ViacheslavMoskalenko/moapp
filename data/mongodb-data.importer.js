const exec = require('child_process').execSync;
const fs = require('fs');
const DefaultDataModifier = require('./default-data.modifier');
const path = require('path');

/**
 * Encapsulates functionality for importing csv data into
 * mongodb
 */
class MongoDbDataImporter {
    /**
     * @param {Object} readProvider an instance of data read provider
     * @param {Object=} dataModifier a modifier instance, which
     */
    constructor(readProvider, dataModifier = new DefaultDataModifier()) {
        this._readProvider = readProvider;
        this._dataModifier = dataModifier;
        this._tempFileName = path.join(__dirname, '__temp_data_to_import.json');
    }
    /**
     * Reads data and returns as a string
     * in a form provided by a provider
     * @param {String} filePath a path to a file
     * @return {Promise<String>} promise to read a file content
     */
    readData(filePath) {
        return this._readProvider.read(filePath);
    }

    /**
     * Modifies a data in place
     * @param {Array<any>} data a list of anything
     * changes each item in data
     */
    modifyData(data) {
        data.forEach(item => this._dataModifier.change(item));
    }

    /**
     * Stores data in a temporary file
     * @param {Array<any>} data data to be stored
     */
    storeData(data) {
        fs.writeFileSync(this._tempFileName, data);
        console.log(`Successfully stored data in a temporary file: ${this._tempFileName}`);
    }

    /**
     * Removes temporary file produced as an output
     * from data storing procedure
     */
    removeData() {
        fs.unlinkSync(this._tempFileName);
    }

    /**
     * Imports data into mongo db
     * @param {String} fileName name of a file to be imported
     */
    async importData(fileName) {
        try {
            const filePath = path.join(__dirname, fileName);
            const data = await this.readData(filePath);
            this.modifyData(data);
            this.storeData(JSON.stringify(data));
            const cmdPrefix = 'mongoimport -c movies -d moapp --drop --mode insert --jsonArray';
            const cmd = `${cmdPrefix} --file ${this._tempFileName}`;
            exec(cmd);
            this.removeData();
            console.log('Data import has been successfully completed');
        } catch (error) {
            console.error('Unable to import data to mongo db: ', error);
        }
    }
}

module.exports = MongoDbDataImporter;
