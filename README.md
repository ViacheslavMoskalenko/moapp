# Movie application
The application provides a possibility to search through movies in a convenient and unobtrusive way.
## Rationale for tech stack:
* angular.js is a good choice for building prototypes fast and it is in LTS mode. Experience - advanced;
* mongodb was chosen instead of relational database, because of possible drastically different movies format. Experience: >= intermediate;
* express - popular node.js based web server, fast and flexible. Experience: >= intermediate
* bootstrap - one of the most popular css frameworks. Also good for rapid prototypes. Experience - >= intermediate

## Trade-offs
Some things were excluded from MVP (minimal viable product):
* Visual statistic (most popular actor, most intensive year, etc) in form of chart diagrams;
* angular.js was chosen because of a prototype nature for this project. Candidates for replacement: React or Angular;
* Client side unit tests are omitted in flavor of e2e tests;
* Design layouts (figma / avocode) were not prepared due to lack of time;
* Server service tests has to close db connection in force mode, which shouldn't happen normally;
* Swagger was replaced with a simple md file, describing data structures for different rest endpoints;
* SEO friendly url was omitted. Options:
    * /year,title
    * /integerId
* Sorting by a column (asc, desc). Records are sorted by name asc by default;
* Streaming data back using mongo db cursors;
* CRUD functionality: adding new movie, updating movie details, removing movie;

## How to run the project on a local machine
* Make sure you have node.js LTS
* Make sure you have MongoDb >= 4
* Run `npm i` in a terminal
* Launch mongodb and run `npm run data:import` to import production data
* Run `npm start` to launch the application
* Run `npm test` to run tests (make sure, that mongod is running, otherwise test will wail)
