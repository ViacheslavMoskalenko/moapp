'use strict';

// Modules
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = function makeWebpackConfig() {
    const config = {
        plugins: []
    };

    config.entry = {
        app: './app/client/src/modules/main/main.module.js'
    };

    config.optimization = {
        splitChunks: {
            // include all types of chunks
            chunks: 'all'
        }
    };

    config.output = {
        path: __dirname + '/app/client/dist',
        filename: '[name].[hash].js',
        chunkFilename: '[name].[hash].js'
    };

    // config.devtool = 'source-map';

    config.module = {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            }, {
                test: /\.(png|jpg|jpeg|gif|svg|woff|woff2|ttf|eot)$/,
                loader: 'file-loader'
            }, {
                test: /\.html$/,
                loader: 'raw-loader'
            }, {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader']
                })
            }, {
                test: /app\/client\/src.*\.js$/,
                use: [
                    {
                        loader: 'ng-annotate-loader?ngAnnotate=ng-annotate-patched'
                    }
                ]
            }
        ]
    };

    config.plugins.push(new ExtractTextPlugin({filename: 'style.css'}));

    config.plugins.push(
        new HtmlWebpackPlugin({
            template: './app/client/src/index.html',
            inject: 'body'
        })
    );

    return config;
}();
