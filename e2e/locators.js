module.exports = {
    movieTitle: '[data-loc="movie-title"]',
    movieItem: '[data-loc="movie-item"]',
    movieClue: '[data-loc="movie-clue"]',
    fetchMore: '[data-loc="fetch-more"]'
};
