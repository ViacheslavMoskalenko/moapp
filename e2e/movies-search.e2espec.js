const puppeteer = require('puppeteer');
const assert = require('assert');
const locators = require('./locators');
module.exports = moviesSearchSpec;

/**
 * Makes sure it is possible to search
 * through movies list
 * @return {Promise<void>}
 */
async function moviesSearchSpec() {
    try {
        const browser = await puppeteer.launch();
        const page = await browser.newPage();
        await page.goto('http://localhost:3000/movies-list');
        console.log('Opened movies search page');
        await page.waitForSelector(locators.movieClue);
        await page.type(locators.movieClue, 'terminator');
        await page.waitForSelector(locators.movieItem);
        console.log('Entered search query');
        const firstResults = await page.evaluate(movieItemSelector => {
            const movies = document.querySelectorAll(movieItemSelector);
            return movies.length;
        }, locators.movieItem);
        await page.click(locators.fetchMore);
        await page.waitFor(500);
        console.log('Clicked "fetch more" button');
        const newResults = await page.evaluate(movieItemSelector => {
            const movies = document.querySelectorAll(movieItemSelector);
            return movies.length;
        }, locators.movieItem);
        console.log('New results: ', newResults);
        console.log('Old results: ', firstResults);
        assert.ok(
            newResults > firstResults,
            'there should be more results after clicking "fetch more" button'
        );
        console.log('Movies search spec: success');
        await browser.close();
    } catch (err) {
        console.error('Movies search spec test failed: ', err);
        throw err;
    }
}
