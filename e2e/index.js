const testMoviesSearch = require('./movies-search.e2espec');
const testMovieDetails = require('./movie-details.e2espec');

(async () => {
    await testMoviesSearch();
    await testMovieDetails();
    console.log('E2e tests done');
})();
