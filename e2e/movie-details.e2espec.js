const puppeteer = require('puppeteer');
const assert = require('assert');
const locators = require('./locators');
module.exports = movieDetailsSpec;

/**
 * Makes sure it is possible to
 * inspect movie details
 * @return {Promise<void>}
 */
async function movieDetailsSpec () {
    try {
        const browser = await puppeteer.launch();
        const page = await browser.newPage();
        await page.goto('http://localhost:3000/movies-list');
        console.log('Opened movies search page');
        await page.waitForSelector(locators.movieClue);
        await page.type(locators.movieClue, 'star');
        console.log('Entered search query');
        await page.waitForSelector(locators.movieItem);
        const secondMovieSelector = `${locators.movieItem}:nth-child(2)`;
        const movieName = await page.evaluate(
            (movieSelector, movieTitleSelector) => {
                const movie = document.querySelector(movieSelector);
                return movie.querySelector(movieTitleSelector).textContent.trim();
            },
            secondMovieSelector,
            locators.movieTitle
        );
        await page.click(`${secondMovieSelector} ${locators.movieTitle}`);
        await page.waitFor(500);
        const movieTitleOnDetailPage = await page.evaluate(movieTitleSelector => {
            return document.querySelector(movieTitleSelector).textContent.trim();
        }, locators.movieTitle);
        assert.ok(movieName === movieTitleOnDetailPage, 'movie title should stay the same');
        console.log('Movie details test: success');
        await browser.close();
    } catch (err) {
        console.error('Movies search spec test failed: ', err);
        throw err;
    }
}
